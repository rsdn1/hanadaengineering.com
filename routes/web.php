<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('portal.home');
// });

Route::get('/', 'PortalController@index')->name('portal.home');
Route::get('/about', 'PortalController@about')->name('portal.about');
Route::get('/service', 'PortalController@service')->name('portal.service');
Route::get('/contact', 'PortalController@contact')->name('portal.contact');

Route::post('/getQuote', 'PortalController@getQuote')->name('getQuote');
Route::post('/getContact', 'PortalController@getContact')->name('getContact');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
