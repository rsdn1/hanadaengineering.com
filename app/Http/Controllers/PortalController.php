<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PortalController extends Controller
{
    public function index()
    {
        return view('portal.home.index');
    }

    public function getQuote(Request $request)
    {
        $data = $request->all();

        $rules = [
            'name'      => 'required',
            'email'     => 'required',
            'number'    => 'required',
            'service'   => 'required',
            'message'   => 'required',
        ];

        $msg = [
            'name.required'      => 'Sila masukan nama',
            'email.required'     => 'Sila masukan e-mel',
            'number.required'    => 'Sila masukan nombor telefon',
            'service.required'   => 'Sila masukan perkhidmatan',
            'message.required'   => 'Sila masukan mesej',
        ];

        $v = Validator::make($data, $rules, $msg);

        if($v->fails())
        {
            return redirect(url('/#quote-form'))
                        // ->url
                        ->withErrors($v)
                        ->withInput();
        }
        else
        {
            $details = [
                'service'   => $request->service,
                'name'      => $request->name,
                'email'     => $request->email,
                'number'    => $request->number,
                'message'   => $request->message,
            ];

            \Mail::to('shuffx92@gmail.com')->send(new \App\Mail\MySendMail($details));

            return redirect(url('/#quote-form'))
                        ->with('success','E-mel berjaya dihantar');
        }
    }

    public function about()
    {
        return view('portal.about.index');
    }

    public function service()
    {
        return view('portal.service.index');
    }

    public function contact()
    {
        return view('portal.contact.index');
    }

    public function getContact(Request $request)
    {
        $data = $request->all();

        $rules = [
            'name'      => 'required',
            'email'     => 'required',
            'subject'   => 'required',
            'message'   => 'required',
        ];

        $msg = [
            'name.required'      => 'Sila masukan nama',
            'email.required'     => 'Sila masukan e-mel',
            'subject.required'   => 'Sila masukan subjek',
            'message.required'   => 'Sila masukan mesej',
        ];

        $v = Validator::make($data, $rules, $msg);

        if($v->fails())
        {
            return redirect(url('/contact/#uformContact1'))
                        ->withErrors($v)
                        ->withInput();
        }
        else
        {
            $details = [
                'name'      => $request->name,
                'email'     => $request->email,
                'subject'   => $request->subject,
                'message'   => $request->message,
            ];

            \Mail::to('shuffx92@gmail.com')->send(new \App\Mail\ContactUs($details));

            return redirect(url('/contact/#uformContact1'))
                        ->with('success','E-mel berjaya dihantar');
        }
    }
}
