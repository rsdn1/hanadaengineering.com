<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Favicon -->
    <link rel="icon" type="image/png" href="assets/img/favicon.png">

    <!-- Title -->
    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Owl Theme Default Min CSS -->
		<link rel="stylesheet" href="{{ asset('css/owl.theme.default.min.css') }}">
		<!-- Owl Carousel Min CSS -->
		<link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
		<!-- Animate Min CSS -->
		<link rel="stylesheet" href="{{ asset('css/animate.min.css') }}">
		<!-- Boxicons Min CSS -->
		<link rel="stylesheet" href="{{ asset('css/boxicons.min.css') }}">
		<!-- Magnific Popup Min CSS -->
		<link rel="stylesheet" href="{{ asset('css/magnific-popup.min.css') }}">
		<!-- Flaticon CSS -->
		<link rel="stylesheet" href="{{ asset('css/flaticon.css') }}">
		<!-- Meanmenu Min CSS -->
		<link rel="stylesheet" href="{{ asset('css/meanmenu.min.css') }}">
		<!-- Nice Select Min CSS -->
		<link rel="stylesheet" href="{{ asset('css/nice-select.min.css') }}">
		<!-- Odometer Min CSS-->
		<link rel="stylesheet" href="{{ asset('css/odometer.min.css') }}">
		<!-- Style CSS -->
		<link rel="stylesheet" href="{{ asset('css/style.css') }}">
		<!-- Responsive CSS -->
		<link rel="stylesheet" href="{{ asset('css/responsive.css') }}">
    </head>

    <body>
		<!-- Start Preloader Area -->
		<div class="preloader">
			<div class="lds-ripple">
				<div></div>
				<div></div>
			</div>
		</div>
		<!-- End Preloader Area -->

		<!-- Start Header Area -->
		<header class="header-area">
			<!-- Start Top Header -->
			@include('components.portal.topHeader')
			<!-- Start Top Header -->

			<!-- Start Nav Area -->
			@include('components.portal.navbar')
			<!-- End Nav Area -->
		</header>
		<!-- End Header Area -->

        @yield('content')

		<!-- Start Footer Area -->
		@include('components.portal.footer')
		<!-- End Footer Area -->

		<!-- Start Copy Right Area -->
        @include('components.portal.copyright')
		<!-- End Copy Right Area -->

		<!-- Start Go Top Area -->
		<div class="go-top">
			<i class="bx bx-chevrons-up"></i>
			<i class="bx bx-chevrons-up"></i>
		</div>
		<!-- End Go Top Area -->


    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
        <!-- Meanmenu Min JS -->
		<script src="{{ asset('js/meanmenu.min.js') }}"></script>
        <!-- Wow Min JS -->
        <script src="{{ asset('js/wow.min.js') }}"></script>
        <!-- Owl Carousel Min JS -->
		<script src="{{ asset('js/owl.carousel.min.js') }}"></script>
        <!-- Nice Select Min JS -->
		<script src="{{ asset('js/nice-select.min.js') }}"></script>
        <!-- Magnific Popup Min JS -->
		<script src="{{ asset('js/magnific-popup.min.js') }}"></script>
		<!-- Parallax Min JS -->
		<script src="{{ asset('js/parallax.min.js') }}"></script>
		<!-- Appear Min JS -->
        <script src="{{ asset('js/appear.min.js') }}"></script>
		<!-- Odometer Min JS -->
		<script src="{{ asset('js/odometer.min.js') }}"></script>
		<!-- Form Validator Min JS -->
		<script src="{{ asset('js/form-validator.min.js') }}"></script>
		<!-- Contact JS -->
		<script src="{{ asset('js/contact-form-script.js') }}"></script>
		<!-- Ajaxchimp Min JS -->
		<script src="{{ asset('js/ajaxchimp.min.js') }}"></script>
        <!-- Custom JS -->
		<script src="{{ asset('js/custom.js') }}"></script>
    </body>
</html>
