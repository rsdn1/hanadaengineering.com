@extends('layouts.portal')

@section('content')
    <!-- Start Page Title Area -->
    @include('portal.about.components.title')
    <!-- End Page Title Area -->

    <!-- Start About Area -->
    @include('portal.about.components.about')
    <!-- End About Area -->

	<!-- Start Our Services Area -->
    @include('portal.about.components.service')
	<!-- End Our Services Area -->

	<!-- Start Testimonial Area -->
    @include('portal.about.components.testimonial')
	<!-- End Testimonial Area -->

    <!-- Start Counter Area -->
    @include('portal.about.components.counter')
    <!-- End Counter Area -->

    <!-- Start Team Area -->
    @include('portal.about.components.team')
    <!-- End Team Area -->
@endsection
