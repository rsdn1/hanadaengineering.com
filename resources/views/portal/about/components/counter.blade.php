<!-- Start Counter Area -->
<section class="counter-area bg-color pt-100 pb-70">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-sm-6">
                <div class="single-counter">
                    <h2>
                        <span class="odometer" data-count="60">00</span>
                    </h2>

                    <h3>Swiming Pool / Fountain</h3>
                </div>
            </div>

            <div class="col-lg-3 col-sm-6">
                <div class="single-counter">
                    <h2>
                        <span class="odometer" data-count="8">00</span>
                    </h2>

                    <h3>Water Treatment Plant</h3>
                </div>
            </div>

            <div class="col-lg-3 col-sm-6">
                <div class="single-counter">
                    <h2>
                        <span class="odometer" data-count="68">00</span>
                    </h2>

                    <h3>Swimming Pool</h3>
                </div>
            </div>

            <div class="col-lg-3 col-sm-6">
                <div class="single-counter">
                    <h2>
                        <span class="odometer" data-count="35">00</span>
                    </h2>

                    <h3>Water Treatment Project</h3>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-sm-6">
                <div class="single-counter">
                    <h2>
                        <span class="odometer" data-count="60">00</span>
                    </h2>

                    <h3>Fountaint Project</h3>
                </div>
            </div>

            <div class="col-lg-3 col-sm-6">
                <div class="single-counter">
                    <h2>
                        <span class="odometer" data-count="8">00</span>
                    </h2>

                    <h3>Water Treatment Servicing</h3>
                </div>
            </div>

            <div class="col-lg-3 col-sm-6">
                <div class="single-counter">
                    <h2>
                        <span class="odometer" data-count="68">00</span>
                    </h2>

                    <h3>Swimming Pool</h3>
                </div>
            </div>

            <div class="col-lg-3 col-sm-6">
                <div class="single-counter">
                    <h2>
                        <span class="odometer" data-count="35">00</span>
                    </h2>

                    <h3>Water Treatment Project & Water Treatment Servicing</h3>
                </div>
            </div>
        </div>
    </div>

    <div class="d-flex justify-content-center">
        <a href="{{ asset('download/companyprofile.pdf') }}" class="default-btn">
            <span>Download Company Profile-2020</span>
        </a>
    </div>

    <div class="shape counter-shape-1">
        <img src="assets/img/counter-shape-1.png" alt="Image">
    </div>
    <div class="shape counter-shape-2">
        <img src="assets/img/counter-shape-2.png" alt="Image">
    </div>
</section>
<!-- End Counter Area -->
