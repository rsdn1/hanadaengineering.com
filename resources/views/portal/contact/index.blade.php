@extends('layouts.portal')

@section('content')
    <!-- Start Page Title Area -->
    <div class="page-title-area bg-15">
        <div class="container">
            <div class="page-title-content">
                <h2>Contact</h2>
                <ul>
                    <li>
                        <a href="index.html">
                            Home
                        </a>
                    </li>
                    <li class="active">Contact</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- End Page Title Area -->

    <!-- Start Contact Area -->
    <section class="main-contact-area ptb-100"  id="uformContact1">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-4">
                    <div class="contact-info">
                        <h3>Our contact details</h3>


                        <ul class="address">
                            <li class="location">
                                <i class="bx bxs-location-plus"></i>
                                <span>Address</span>
                                Lot 041B, Lorong bIstari 1, kg sri aman puchong
                            </li>
                            <li>
                                <i class="bx bxs-phone-call"></i>
                                <span>Phone</span>
                                <a href="+603-80611041">+603-80611041</a>

                            </li>
                            <li>
                                <i class="bx bxs-envelope"></i>
                                <span>Email</span>
                                <a href="mailto:hanadaservices@gmail.com">hanadaservices@gmail.com</a>

                            </li>
                        </ul>

                        <div class="sidebar-follow-us">
                            <h3>Follow us:</h3>

                            <ul class="social-wrap">
                                <li>
                                    <a href="#" target="_blank">
                                        <i class="bx bxl-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" target="_blank">
                                        <i class="bx bxl-instagram"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" target="_blank">
                                        <i class="bx bxl-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" target="_blank">
                                        <i class="bx bxl-youtube"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-lg-8">
                    <div class="contact-wrap">
                        <div class="contact-form">
                            <div class="contact-title">
                                <h2>Ready to get started?</h2>
                            </div>

                            @if ($message = Session::get('success'))
                                <div class="alert alert-success">
                                    {{ $message }}
                                </div>
                            @endif

                            <form id="contactForm1" action="{{ route('getContact') }}" method="POST">
                                @csrf
                                <div class="row">
                                    <div class="col-lg-6 col-sm-6">
                                        <div class="form-group">
                                            <label>Name</label>
                                            <input type="text" name="name" id="name"
                                                class="form-control @error('name') is-invalid @enderror">
                                            @error('name')
                                                <span class="help-block with-errors" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-lg-6 col-sm-6">
                                        <div class="form-group">
                                            <label>Email Address</label>
                                            <input type="email" name="email" id="email"
                                                class="form-control @error('email') is-invalid @enderror">
                                            @error('email')
                                                <span class="help-block with-errors" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-12">
                                        <div class="form-group">
                                            <label>Subject</label>
                                            <input type="text" name="subject" id="msg_subject"
                                                class="form-control @error('subject') is-invalid @enderror">
                                            @error('subject')
                                                <span class="help-block with-errors" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-12">
                                        <div class="form-group">
                                            <label>Message</label>
                                            <textarea name="message"
                                                class="form-control @error('message') is-invalid @enderror" id="message"
                                                cols="30" rows="10"></textarea>
                                            @error('message')
                                                <span class="help-block with-errors" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-md-12">
                                        <button type="submit" class="default-btn">
                                            <span>Send message</span>
                                        </button>
                                        {{-- <div id="msgSubmit" class="h3 text-center hidden"></div>
                                        <div class="clearfix"></div> --}}
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Contact Area -->

    <!-- Start Map Area -->
    <div class="map-area">
        <iframe
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3984.3437336297416!2d101.5841763147569!3d3.001832197813825!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31cdb30e904de29d%3A0xe173d2099d4ca8f2!2s41%2C%20Lorong%20Bistari%201%2C%20Kampung%20Sri%20Aman%20Bistari%2C%2047150%20Puchong%2C%20Selangor!5e0!3m2!1sms!2smy!4v1627715243551!5m2!1sms!2smy"
            width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
    </div>
    <!-- End Map Area -->
@endsection
