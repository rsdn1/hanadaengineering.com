@extends('layouts.portal')

@section('content')
    <!-- Start Page Title Area -->
    <div class="page-title-area bg-19">
        <div class="container">
            <div class="page-title-content">
                <h2>Services details</h2>
                <ul>
                    <li>
                        <a href="index.html">
                            Home
                        </a>
                    </li>
                    <li class="active">Services details</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- End Page Title Area -->

    <!-- Start Services Details Area -->
    <section class="services-details-area pt-100 pb-70">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="services-details-content">
                        <div class="services-contents">
                            <div class="services-details-one">
                                <img src="assets/img/services-details/services-details-1.jpg" alt="Image">
                            </div>

                            <h2>Pool cleaning services</h2>
                        </div>

                        <div class="services-contents">
                            <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel
                                illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui
                                blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem
                                ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt
                                ut laoreet dolore magna aliquam erat volutpat. eum iriure dolor in</p>

                            <p>feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent
                                luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit
                                amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet
                                dolore magna aliquam erat volutpat. eum iriure dolor in</p>
                        </div>

                        <div class="services-contents">
                            <h3>We want to clean your pool!</h3>
                            <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel
                                illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui
                                blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem
                                ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt
                                ut laoreet dolore magna aliquam erat volutpat. nulla facilisi.</p>

                            <ul>
                                <li>Brush & vacuum</li>
                                <li>Skimmer & pump basket cleaning</li>
                                <li>Water chemistry testing</li>
                                <li>Chemical additions & delivery</li>
                                <li>Electronic service completion reports</li>
                            </ul>
                        </div>

                        <div class="services-contents">
                            <h3>Experienced pool cleaning professionals</h3>
                            <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel
                                illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui
                                blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem
                                ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt
                                ut laoreet dolore magna aliquam erat volutpat. eum iriure dolor in</p>

                            <p>feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent
                                luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit
                                amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet
                                dolore magna aliquam erat volutpat. eum iriure dolor in</p>
                        </div>

                        <div class="services-contents">
                            <h3>Schedule your regular pool cleaning today</h3>
                            <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel
                                illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui
                                blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem
                                ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt
                                ut laoreet dolore magna aliquam erat volutpat. eum iriure dolor in</p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="services-sidebar">
                        <div class="services-category">
                            <ul>
                                <li>
                                    <a href="#">
                                        Pool Cleaning
                                        <i class="bx bx-chevron-right"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Pool Services
                                        <i class="bx bx-chevron-right"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Pool Renovation
                                        <i class="bx bx-chevron-right"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Pool Construction
                                        <i class="bx bx-chevron-right"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Pool Maintenance
                                        <i class="bx bx-chevron-right"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Pool Refurbishment
                                        <i class="bx bx-chevron-right"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Pool Remodeling
                                        <i class="bx bx-chevron-right"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <div class="address">
                            <h3>Contact us today</h3>
                            <ul>
                                <li class="location">
                                    <span>Address</span>
                                    :   Lot 041B, Lorong bIstari 1, kg sri aman puchong
                                </li>
                                <li>
                                    <span>Email</span>
                                    <a href="mailto:hanadaservices@gmail.com">hanadaservices@gmail.com</a>
                                </li>
                                <li>
                                    <span>Call us</span>
                                    <a href="+603-80611041">+603-80611041</a>
                                </li>
                                <li>
                                    <span>Mon - Fri</span>
                                    : 09:00 AM - 05:30 PM
                                </li>
                                <li>
                                    <span>Saturday</span>
                                    : 09:00 AM - 04:00 PM
                                </li>
                                <li>
                                    <span>Sunday</span>
                                    : Closed
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Services Details Area -->
@endsection
