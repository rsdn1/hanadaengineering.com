<!-- Start About Area -->
<section class="about-area about-area-three">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6 p-0">
                <div class="about-bg-img">
                    <div class="video-btn-2">
                        <a href="https://www.youtube.com/watch?v=1Ue9I-HuNzw" class="popup-youtube">
                            <span></span>
                            <span></span>
                            <span></span>
                            <span></span>
                            <i class="flaticon-play-button"></i>
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 p-0">
                <div class="about-content-bg">
                    <div class="about-content">
                        <span class="top-title">Who we are</span>
                        <h2>We are a friendly and professional pool services & Design, build and maintenance for pool</h2>


                        <a href="{{ route('portal.service') }}"  class="default-btn">
                            <span>Read more</span>
                        </a>
                    </div>

                    <div class="about-shape">
                        <img src="{{ asset('img/about-shape.png') }}" alt="Image">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End About Area -->
