<!-- Start Our Services Area -->
<section class="our-services-area our-service-area-three pt-100 pb-70">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-sm-6">
                <div class="services-content">
                    <span class="top-title">Our services</span>
                    <h2>All the quality service that we provide to our clients.</h2>

                    <a href="{{ route('portal.service') }}" class="default-btn">
                        <span>All services</span>
                    </a>
                </div>
            </div>

            <div class="col-lg-4 col-sm-6">
                <div class="single-services bg-1">
                    <span class="flaticon-swimming-pool"></span>
                    <h3>Pool refurbishment</h3>
                     <p></p>
                </div>
            </div>

            <div class="col-lg-4 col-sm-6">
                <div class="single-services bg-2">
                    <span class="flaticon-swimming-pool-3"></span>
                    <h3>Building Swimming Pool</h3>
                    <p></p>
                </div>
            </div>

            <div class="col-lg-4 col-sm-6">
                <div class="single-services bg-3">
                    <span class="flaticon-temperature-control"></span>
                    <h3>Building Spring Water</h3>
                    <p></p>
                </div>
            </div>

            <div class="col-lg-4 col-sm-6">
                <div class="single-services bg-4">
                    <span class="flaticon-swimming-pool-2"></span>
                    <h3>Supplying Swimming Pool Equipment & Chemicals </h3>
                    <p></p>
                </div>
            </div>

            <div class="col-lg-4 col-sm-6">
                <div class="single-services bg-5">
                    <span class="flaticon-swimming-pool-1"></span>
                    <h3> Water Plant System Maintenance </h3>
                    <p></p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Our Services Area -->
