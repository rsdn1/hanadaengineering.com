<!-- Start Our Services Area -->
<section class="our-services-area our-services-area-three bg-color pt-100 pb-70">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-sm-6">
                <div class="single-services">
                    <a href="services-details.html">
                        <img src="{{ asset('img/services/services-1.jpg') }}" alt="Image">
                    </a>

                    <div class="services-content">
                        <h3>
                            <a href="{{ route('portal.service') }}">
                                Pool refurbishment
                            </a>
                        </h3>

                        <p></p>

                        <a href="{{ route('portal.service') }}" class="read-more">
                            Read more
                            <i class="flaticon-right-arrow"></i>
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-sm-6">
                <div class="single-services">
                    <a href="services-details.html">
                        <img src="{{ asset('img/services/services-2.jpg') }}" alt="Image">
                    </a>

                    <div class="services-content">
                        <h3>
                            <a href="{{ route('portal.service') }}">
                                Building Swimming Pool
                            </a>
                        </h3>

                        <p></p>

                        <a href="{{ route('portal.service') }}" class="read-more">
                            Read more
                            <i class="flaticon-right-arrow"></i>
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-sm-6 offset-sm-3 offset-lg-0">
                <div class="single-services">
                    <a href="services-details.html">
                        <img src="{{ asset('img/services/services-3.jpg') }}" alt="Image">
                    </a>

                    <div class="services-content">
                        <h3>
                            <a href="{{ route('portal.service') }}">
                                Pool cleaning
                            </a>
                        </h3>

                        <p></p>

                        <a href="{{ route('portal.service') }}" class="read-more">
                            Read more
                            <i class="flaticon-right-arrow"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Our Services Area -->
