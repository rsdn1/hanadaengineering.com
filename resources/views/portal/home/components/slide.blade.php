  <!-- Start Hero Slider Area -->
  <section class="hero-slider-area">
      <div class="hero-slider owl-carousel owl-theme">
          <div class="slider-item slider-item-bg-1">
              <div class="d-table">
                  <div class="d-table-cell">
                      <div class="container">
                          <div class="slider-text">

                              <h1>We work with pool services & renovation</h1>
                              <p>Design, build and maintenance for pool , water features and others ( water system).</p>

                              <div class="slider-btn">
                                  <a class="default-btn" href="{{ route('portal.contact') }}" >
                                      <span>Contact Us</span>
                                  </a>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>

          <div class="slider-item slider-item-bg-2">
              <div class="d-table">
                  <div class="d-table-cell">
                      <div class="container">
                          <div class="slider-text">

                              <h1>Design, build and maintenance for pool , water features and others ( water system)</h1>


                              <div class="slider-btn">
                                <a class="default-btn" href="#get" >
                                      <span>Request a quote</span>
                                  </a>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </section>
  <!-- End Hero Slider Area -->
