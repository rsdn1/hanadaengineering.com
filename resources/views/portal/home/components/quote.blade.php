<!-- Start We Are Area -->
<section id="quote-form" class="we-are-area we-are-area-three ptb-100">
    <div class="container">
        <form class="we-area-form" action="{{ route('getQuote') }}" method="POST">
            @csrf
            <div class="we-area-title">
                <h2>Get a free quote!</h2>
            </div>

            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    {{ $message }}
                </div>
            @endif

            <div class="row">
                <div class="col-lg-6 col-sm-6">
                    <div class="form-group">
                        <input type="text" id="get" class="form-control @error('name') is-invalid @enderror" id="First-Name" placeholder="Name" name="name">
                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="col-lg-6 col-sm-6">
                    <div class="form-group">
                        <input type="email" class="form-control @error('email') is-invalid @enderror" id="Email" placeholder="Email" name="email">
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="col-lg-6 col-sm-6">
                    <div class="form-group">
                        <input type="text" class="form-control @error('number') is-invalid @enderror" id="Number" placeholder="Phone number" name="number">
                        @error('number')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="col-lg-6 col-sm-6">
                    <div class="form-group">
                        <select name="service" class="@error('service') is-invalid @enderror">
                            <option value="">Select your services</option>
                            <option>Pool Services</option>
                            <option>Food Services</option>
                            <option>Swimming Services</option>
                            <option>Water Services</option>
                        </select>
                        @error('service')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="form-group">
                        <textarea name="message" class="form-control @error('message') is-invalid @enderror" id="Message" cols="30" rows="5" placeholder="Message"></textarea>
                        @error('message')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
            </div>

            <button type="submit" class="default-btn">
                <span>Send message</span>
            </button>
        </form>
    </div>
</section>
<!-- End We Are Area -->
