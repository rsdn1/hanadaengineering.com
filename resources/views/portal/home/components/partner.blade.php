<!-- Start Partner Area -->
<div class="partner-area bg-color ptb-100">
    <div class="container">
        <span class="top-title">Our Clients</span>
        <br>
        <br>
        <br>
        <div class="partner-slider owl-theme owl-carousel">
            <div class="partner-item">

                    <img src="{{ asset('img/client/1.png') }}" alt="Image">
                </a>
            </div>

            <div class="partner-item">
                <a href="#">
                    <img src="{{ asset('img/client/2.png') }}" alt="Image">
                </a>
            </div>

            <div class="partner-item">
                <a href="#">
                    <img src="{{ asset('img/client/3.png') }}" alt="Image">
                </a>
            </div>

            <div class="partner-item">
                <a href="#">
                    <img src="{{ asset('img/client/4.png') }}" alt="Image">
                </a>
            </div>

            <div class="partner-item">
                <a href="#">
                    <img src="{{ asset('img/client/5.png') }}" alt="Image">
                </a>
            </div>
            <div class="partner-item">
                <a href="#">
                    <img src="{{ asset('img/client/6.png') }}" alt="Image">
                </a>
            </div>
            <div class="partner-item">
                <a href="#">
                    <img src="{{ asset('img/client/7.png') }}" alt="Image">
                </a>
            </div>
            <div class="partner-item">
                <a href="#">
                    <img src="{{ asset('img/client/8.png') }}" alt="Image">
                </a>
            </div>
            <div class="partner-item">
                <a href="#">
                    <img src="{{ asset('img/client/9.png') }}" alt="Image">
                </a>
            </div>
            <div class="partner-item">
                <a href="#">
                    <img src="{{ asset('img/client/10.png') }}" alt="Image">
                </a>
            </div>
            <div class="partner-item">
                <a href="#">
                    <img src="{{ asset('img/client/11.png') }}" alt="Image">
                </a>
            </div>
            <div class="partner-item">
                <a href="#">
                    <img src="{{ asset('img/client/12.png') }}" alt="Image">
                </a>
            </div>
            <div class="partner-item">
                <a href="#">
                    <img src="{{ asset('img/client/13.png') }}" alt="Image">
                </a>
            </div>
            <div class="partner-item">
                <a href="#">
                    <img src="{{ asset('img/client/14.png') }}" alt="Image">
                </a>
            </div>
            <div class="partner-item">
                <a href="#">
                    <img src="{{ asset('img/client/15.png') }}" alt="Image">
                </a>
            </div>
            <div class="partner-item">
                <a href="#">
                    <img src="{{ asset('img/client/16.png') }}" alt="Image">
                </a>
            </div>
            <div class="partner-item">
                <a href="#">
                    <img src="{{ asset('img/client/17.png') }}" alt="Image">
                </a>
            </div>
            <div class="partner-item">
                <a href="#">
                    <img src="{{ asset('img/client/18.png') }}" alt="Image">
                </a>
            </div>
            <div class="partner-item">
                <a href="#">
                    <img src="{{ asset('img/client/19.png') }}" alt="Image">
                </a>
            </div>
            <div class="partner-item">
                <a href="#">
                    <img src="{{ asset('img/client/20.png') }}" alt="Image">
                </a>
            </div>
            <div class="partner-item">
                <a href="#">
                    <img src="{{ asset('img/client/21.png') }}" alt="Image">
                </a>
            </div>
            <div class="partner-item">
                <a href="#">
                    <img src="{{ asset('img/client/22.png') }}" alt="Image">
                </a>
            </div>
            <div class="partner-item">
                <a href="#">
                    <img src="{{ asset('img/client/23.png') }}" alt="Image">
                </a>
            </div>
            <div class="partner-item">
                <a href="#">
                    <img src="{{ asset('img/client/24.png') }}" alt="Image">
                </a>
            </div>
            <div class="partner-item">
                <a href="#">
                    <img src="{{ asset('img/client/25.png') }}" alt="Image">
                </a>
            </div>
            <div class="partner-item">
                <a href="#">
                    <img src="{{ asset('img/client/26.png') }}" alt="Image">
                </a>
            </div>
            <div class="partner-item">
                <a href="#">
                    <img src="{{ asset('img/client/27.png') }}" alt="Image">
                </a>
            </div>
            <div class="partner-item">
                <a href="#">
                    <img src="{{ asset('img/client/28.png') }}" alt="Image">
                </a>
            </div>
            <div class="partner-item">
                <a href="#">
                    <img src="{{ asset('img/client/29.png') }}" alt="Image">
                </a>
            </div>
            <div class="partner-item">
                <a href="#">
                    <img src="{{ asset('img/client/30.png') }}" alt="Image">
                </a>
            </div>
        </div>
    </div>
</div>
<!-- End Partner Area -->
