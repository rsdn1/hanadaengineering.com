<!-- Start Pricing Area -->
{{-- <section class="pricing-area pt-100 pb-70">
    <div class="container">
        <div class="section-title">
            <span>Pricing plans</span>
            <h2>Pool service plans & packages</h2>
        </div>

        <div class="row">
            <div class="col-lg-4 col-sm-6">
                <div class="single-pricing">
                    <h3>Basic plan</h3>
                    <h2>29<Sub>$</Sub></h2>
                    <span class="plans">Weekly pool service plans</span>

                    <ul>
                        <li>Pool refurbishment</li>
                        <li>Pool cleaning</li>
                        <li>Pool renovation</li>
                        <li>Equipment inspection</li>
                    </ul>

                    <a href="#" class="default-btn">
                        <span>Choose plan</span>
                    </a>
                </div>
            </div>

            <div class="col-lg-4 col-sm-6">
                <div class="single-pricing active">
                    <h3>Ultra plan</h3>
                    <h2>99<Sub>$</Sub></h2>
                    <span class="plans">Monthly pool service plans</span>

                    <ul>
                        <li>Pool refurbishment</li>
                        <li>Pool cleaning</li>
                        <li>Pool renovation</li>
                        <li>Equipment inspection</li>
                    </ul>

                    <a href="#" class="default-btn">
                        <span>Choose plan</span>
                    </a>

                    <span class="recommended">Recommended</span>
                </div>
            </div>

            <div class="col-lg-4 col-sm-6 offset-sm-3 offset-lg-0">
                <div class="single-pricing">
                    <h3>Enterprise plan</h3>
                    <h2>999<Sub>$</Sub></h2>
                    <span class="plans">Yearly pool service plans</span>

                    <ul>
                        <li>Pool refurbishment</li>
                        <li>Pool cleaning</li>
                        <li>Pool renovation</li>
                        <li>Equipment inspection</li>
                    </ul>

                    <a href="#" class="default-btn">
                        <span>Choose plan</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section> --}}
<!-- End Pricing Area -->
