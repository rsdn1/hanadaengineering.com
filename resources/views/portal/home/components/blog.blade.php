<!-- Start Blog Area -->
{{-- <section class="blog-area pt-100 pb-70">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-8">
                <div class="section-title left-title">
                    <span>Recent news</span>
                    <h2>Success story posts</h2>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="section-btn">
                    <a href="blog-column-one.html" class="default-btn">
                        <span>View all</span>
                    </a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="single-blog-post">
                    <div class="blog-img">
                        <a href="blog-details.html">
                            <img src="{{ asset('img/blog/blog-1.jpg') }}" alt="Image">
                        </a>
                    </div>

                    <div class="news-content">
                        <h3>
                            <a href="blog-details.html">5 Effective ways to keep your swimming pool clean</a>
                        </h3>

                        <ul>
                            <li>
                                <a href="#">Andrew lawson</a>
                            </li>
                            <li>
                                <span>28-09-2020</span>
                            </li>
                        </ul>

                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod...</p>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6">
                <div class="single-blog-post">
                    <div class="blog-img">
                        <a href="blog-details.html">
                            <img src="{{ asset('img/blog/blog-2.jpg') }}" alt="Image">
                        </a>
                    </div>

                    <div class="news-content">
                        <h3>
                            <a href="blog-details.html">How can I make my pool ecologically friendly?</a>
                        </h3>

                        <ul>
                            <li>
                                <a href="#">Lawson Eugene</a>
                            </li>
                            <li>
                                <span>29-09-2020</span>
                            </li>
                        </ul>

                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod...</p>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 offset-md-3 offset-lg-0">
                <div class="single-blog-post">
                    <div class="blog-img">
                        <a href="blog-details.html">
                            <img src="{{ asset('img/blog/blog-3.jpg') }}" alt="Image">
                        </a>
                    </div>

                    <div class="news-content">
                        <h3>
                            <a href="blog-details.html">Plan the perfect backyard pool renovation</a>
                        </h3>

                        <ul>
                            <li>
                                <a href="#">Angel james </a>
                            </li>
                            <li>
                                <span>30-09-2020</span>
                            </li>
                        </ul>

                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod...</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> --}}
<!-- Eed Blog Area -->
