{{-- <!-- Strat Contact Info Area -->
<section class="contact-info-area pt-100">
    <div class="container">
        <div class="contact-info-bg">
            <div class="row">
                <div class="col-lg-4 col-sm-6">
                    <div class="single-contact-info">
                        <i class="flaticon-location"></i>
                        <h3>Visit us</h3>
                        <span>6890 Blvd, The Bronx, NY <br> 1058 New York, USA</span>
                    </div>
                </div>

                <div class="col-lg-4 col-sm-6">
                    <div class="single-contact-info">
                        <i class="flaticon-24-hours-support"></i>
                        <h3>Call us</h3>
                        <a href="tel:+1-(514)-312-5678">+1 (514) 312-5678</a>
                        <a href="tel:+1-(514)-312-6688">+1 (514) 312-6688</a>
                    </div>
                </div>

                <div class="col-lg-4 col-sm-6 offset-sm-3 offset-lg-0">
                    <div class="single-contact-info">
                        <i class="flaticon-email"></i>
                        <h3>Visit us</h3>
                        <a href="mailto:hello@saspol.com">hello@saspol.com</a>
                        <a href="#">Skype: saspol</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Contact Info Area --> --}}
