<!-- Start Gallery Area -->
<section class="gallery-area gallery-area-three bg-color gallery-popup pt-100 pb-70">
    <div class="container-fluid">
        <div class="section-title left-title white-title">
            <span>Our gallery</span>
            <h2>Amazing photo gallery</h2>
        </div>
        <div class="gallery-slider-three owl-theme owl-carousel">
            <div class="single-gallery">
                <img src="{{ asset('img/gallery/gallery-1.jpg') }}" alt="Image">

                <a href="{{ asset('img/gallery/gallery-1.jpg') }}" class="view-gallery">
                    <i class='bx bx-show-alt'></i>
                </a>
            </div>

            <div class="single-gallery">
                <img src="{{ asset('img/gallery/gallery-2.jpg') }}" alt="Image">

                <a href="{{ asset('img/gallery/gallery-2.jpg') }}" class="view-gallery">
                    <i class='bx bx-show-alt'></i>
                </a>
            </div>

            <div class="single-gallery">
                <img src="{{ asset('img/gallery/gallery-3.jpg') }}" alt="Image">

                <a href="{{ asset('img/gallery/gallery-3.jpg') }}" class="view-gallery">
                    <i class='bx bx-show-alt'></i>
                </a>
            </div>

            <div class="single-gallery">
                <img src="{{ asset('img/gallery/gallery-4.jpg') }}" alt="Image">

                <a href="{{ asset('img/gallery/gallery-4.jpg') }}" class="view-gallery">
                    <i class='bx bx-show-alt'></i>
                </a>
            </div>
        </div>
    </div>

    <div class="gallery-shape-3">
        <img src="{{ asset('img/gallery/gallery-shape-3.png') }}" alt="Image">
    </div>
</section>
<!-- End Gallery Area -->
