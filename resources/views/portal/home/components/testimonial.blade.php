{{-- <!-- Start Testimonial Area -->
<section class="testimonial-area testimonial-area-three bg-color ptb-100">
    <div class="container">
        <div class="testimonial-title">
            <i class="flaticon-employees"></i>
            <h4>What our customers say</h4>
        </div>

        <div class="testimonials-slider owl-theme owl-carousel">
            <div class="testimonial-content">
                <i class="flaticon-straight-quotes"></i>
                <p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.”</p>
                <h3>Donald sheetz</h3>
                <span>Client of company</span>
            </div>

            <div class="testimonial-content">
                <i class="flaticon-straight-quotes"></i>
                <p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.”</p>
                <h3>Anna Deo</h3>
                <span>Client of company</span>
            </div>

            <div class="testimonial-content">
                <i class="flaticon-straight-quotes"></i>
                <p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.”</p>
                <h3>Kilvis Smith</h3>
                <span>Client of company</span>
            </div>
        </div>
    </div>

    <div class="testimonial-shape-1">
        <img src="{{ asset('img/testimonial-shape-1.png') }}" alt="Image">
    </div>
    <div class="testimonial-shape-2">
        <img src="{{ asset('img/testimonial-shape-2.png') }}" alt="Image">
    </div>
</section>
<!-- End Testimonial Area --> --}}
