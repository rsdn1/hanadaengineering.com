{{-- <!-- Start Subscribe Area -->
<section class="subscribe-area">
    <div class="container ptb-100">
        <div class="row align-items-center">
            <div class="col-lg-3 col-md-4">
                <div class="subscribe-content">
                    <span>SUBSCRIBE TO OUR</span>
                    <h2>Newsletter</h2>
                </div>
            </div>

            <div class="col-lg-9 col-md-8">
                <form class="newsletter-form" data-toggle="validator">
                    <input type="email" class="form-control" placeholder="Enter email address" name="EMAIL" required autocomplete="off">

                    <button class="default-btn" type="submit">
                        <span>Subscribe now</span>
                    </button>

                    <div id="validator-newsletter" class="form-result"></div>
                </form>
            </div>
        </div>
    </div>
</section>
<!-- End Subscribe Area --> --}}
