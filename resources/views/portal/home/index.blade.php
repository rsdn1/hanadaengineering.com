@extends('layouts.portal')

@section('content')
		<!-- Start Hero Slider Area -->
        @include('portal.home.components.slide')
		<!-- End Hero Slider Area -->

		<!-- Start Our Services Area -->
		@include('portal.home.components.service')
		<!-- End Our Services Area -->

		<!-- Start About Area -->
		@include('portal.home.components.about')
		<!-- End About Area -->

		<!-- Start Partner Area -->
		@include('portal.home.components.partner')
		<!-- End Partner Area -->

		<!-- Start Our Services Area -->
		@include('portal.home.components.serviceLink')
		<!-- End Our Services Area -->

		<!-- Start Gallery Area -->
		@include('portal.home.components.gallery')
		<!-- End Gallery Area -->

		<!-- Start Pricing Area -->
		@include('portal.home.components.pricing')
		<!-- End Pricing Area -->

		<!-- Start We Are Area -->
		@include('portal.home.components.quote')
		<!-- End We Are Area -->

		<!-- Start Team Area -->
        @include('portal.home.components.team')
		<!-- End Team Area -->

		<!-- Start Testimonial Area -->
		@include('portal.home.components.testimonial')
		<!-- End Testimonial Area -->

		<!-- Strat Contact Info Area -->
		@include('portal.home.components.contact')
		<!-- End Contact Info Area -->

		<!-- Start Blog Area -->
		@include('portal.home.components.blog')
		<!-- Eed Blog Area -->

		<!-- Start Subscribe Area -->
		@include('portal.home.components.subscribe')
		<!-- End Subscribe Area -->
@endsection
