<!-- Start Nav Area -->
<div class="navbar-area">
    <div class="mobile-nav">
        <div class="container-fluid">
            <a href="index.html" class="logo">
                <img src="{{ asset('img/logo2.png') }}" alt="Logo">
            </a>
        </div>
    </div>

    <div class="main-nav">
        <div class="container-fluid">
            <nav class="navbar navbar-expand-md">
                <a class="navbar-brand" href="index.html">
                    <img src="{{ asset('img/logo2.png') }}" alt="Logo">
                </a>

                <div class="collapse navbar-collapse mean-menu">
                    <ul class="navbar-nav m-auto">
                        <li class="nav-item">
                            <a href="{{ route('portal.home') }}" class="nav-link {{ (request()->is('/')) ? 'active' : '' }}">
                                Home
                                {{-- <i class="bx bx-chevron-down"></i> --}}
                            </a>

                            {{-- <ul class="dropdown-menu">
                                <li class="nav-item">
                                    <a href="index.html" class="nav-link">Home One</a>
                                </li>
                                <li class="nav-item">
                                    <a href="index-2.html" class="nav-link">Home Two</a>
                                </li>
                                <li class="nav-item">
                                    <a href="index-3.html" class="nav-link active">Home Three</a>
                                </li>
                            </ul> --}}
                        </li>

                        <li class="nav-item">
                            <a href="{{ route('portal.about') }}" class="nav-link {{ (request()->is('about')) ? 'active' : '' }}">About Us</a>
                        </li>

                        {{-- <li class="nav-item">
                            <a href="#" class="nav-link">
                                Services
                                <i class="bx bx-chevron-down"></i>
                            </a>

                            <ul class="dropdown-menu">
                                <li class="nav-item">
                                    <a href="services-style-one.html" class="nav-link">Services Style One</a>
                                </li>
                                <li class="nav-item">
                                    <a href="services-style-two.html" class="nav-link">Services Style Two</a>
                                </li>
                                <li class="nav-item">
                                    <a href="services-style-three.html" class="nav-link">Services Style Three</a>
                                </li>
                                <li class="nav-item">
                                    <a href="services-details.html" class="nav-link">Service Details</a>
                                </li>
                            </ul>
                        </li> --}}

                        {{-- <li class="nav-item">
                            <a href="#" class="nav-link">
                                Pages
                                <i class="bx bx-chevron-down"></i>
                            </a>

                            <ul class="dropdown-menu">
                                <li class="nav-item">
                                    <a href="team.html" class="nav-link">Team</a>
                                </li>

                                <li class="nav-item">
                                    <a href="#" class="nav-link">
                                        Gallery
                                        <i class="bx bx-chevron-right"></i>
                                    </a>

                                    <ul class="dropdown-menu">
                                        <li class="nav-item">
                                            <a href="gallery-column-two.html" class="nav-link">Gallery Column Two</a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="gallery-column-three.html" class="nav-link">Gallery Column Three</a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="gallery-column-four.html" class="nav-link">Gallery Column Four</a>
                                        </li>
                                    </ul>
                                </li>

                                <li class="nav-item">
                                    <a href="testimonials.html" class="nav-link">Testimonials</a>
                                </li>

                                <li class="nav-item">
                                    <a href="#" class="nav-link">
                                        Pricing
                                        <i class="bx bx-chevron-right"></i>
                                    </a>

                                    <ul class="dropdown-menu">
                                        <li class="nav-item">
                                            <a href="pricing-style-one.html" class="nav-link">Pricing Style One</a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="pricing-style-two.html" class="nav-link">Pricing Style Two</a>
                                        </li>
                                    </ul>
                                </li>

                                <li class="nav-item">
                                    <a href="faq.html" class="nav-link">FAQ</a>
                                </li>

                                <li class="nav-item">
                                    <a href="#" class="nav-link">
                                        User
                                        <i class="bx bx-chevron-right"></i>
                                    </a>

                                    <ul class="dropdown-menu">
                                        <li class="nav-item">
                                            <a href="my-account.html" class="nav-link">My Account</a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="log-in.html" class="nav-link">Log In</a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="registration.html" class="nav-link">Registration</a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="recover-password.html" class="nav-link">Recover Password</a>
                                        </li>
                                    </ul>
                                </li>

                                <li class="nav-item">
                                    <a href="privacy-policy.html" class="nav-link">Privacy Policy</a>
                                </li>
                                <li class="nav-item">
                                    <a href="terms-conditions.html" class="nav-link">Terms & Conditions</a>
                                </li>
                                <li class="nav-item">
                                    <a href="coming-soon.html" class="nav-link">Coming Soon</a>
                                </li>
                                <li class="nav-item">
                                    <a href="404.html" class="nav-link">404 Error Page</a>
                                </li>
                            </ul>
                        </li> --}}

                        {{-- <li class="nav-item">
                            <a href="#" class="nav-link">
                                Shop
                                <i class="bx bx-chevron-down"></i>
                            </a>

                            <ul class="dropdown-menu">
                                <li class="nav-item">
                                    <a href="products.html" class="nav-link">Products</a>
                                </li>
                                <li class="nav-item">
                                    <a href="shopping-cart.html" class="nav-link">Shopping Cart</a>
                                </li>
                                <li class="nav-item">
                                    <a href="checkout.html" class="nav-link">Checkout</a>
                                </li>
                                <li class="nav-item">
                                    <a href="product-details.html" class="nav-link">Product Details</a>
                                </li>
                                <li class="nav-item">
                                    <a href="wishlist.html" class="nav-link">Wishlist</a>
                                </li>
                            </ul>
                        </li> --}}

                        {{-- <li class="nav-item">
                            <a href="#" class="nav-link">
                                Blog
                                <i class="bx bx-chevron-down"></i>
                            </a>

                            <ul class="dropdown-menu">
                                <li class="nav-item">
                                    <a href="blog-column-one.html" class="nav-link">Blog Column One</a>
                                </li>
                                <li class="nav-item">
                                    <a href="blog-column-two.html" class="nav-link">Blog Column Two</a>
                                </li>
                                <li class="nav-item">
                                    <a href="blog-left-sidebar.html" class="nav-link">Blog Left Sidebar</a>
                                </li>
                                <li class="nav-item">
                                    <a href="blog-details.html" class="nav-link">Blog Details</a>
                                </li>
                            </ul>
                        </li> --}}

                        <li class="nav-item">
                            <a href="{{ route('portal.service') }}" class="nav-link {{ (request()->is('service')) ? 'active' : '' }}">Service</a>
                        </li>

                        <li class="nav-item">
                            <a href="{{ route('portal.contact') }}" class="nav-link {{ (request()->is('contact')) ? 'active' : '' }}">Contact Us</a>
                        </li>
                    </ul>

                    {{-- <div class="others-option">
                        <div class="option-item">
                            <i class="search-btn bx bx-search"></i>
                            <i class="close-btn bx bx-x"></i>

                            <div class="search-overlay search-popup">
                                <div class='search-box'>
                                    <form class="search-form">
                                        <input class="search-input" name="search" placeholder="Search" type="text">

                                        <button class="search-button" type="submit">
                                            <i class="bx bx-search"></i>
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="cart-icon">
                            <a href="shopping-cart.html">
                                <i class="bx bx-cart"></i>
                                <span>0</span>
                            </a>
                        </div>
                    </div> --}}
                </div>
            </nav>
        </div>
    </div>

    {{-- <div class="others-option-for-responsive">
        <div class="container">
            <div class="dot-menu">
                <div class="inner">
                    <div class="circle circle-one"></div>
                    <div class="circle circle-two"></div>
                    <div class="circle circle-three"></div>
                </div>
            </div>

            <div class="container">
                <div class="option-inner">
                    <div class="others-option justify-content-center d-flex align-items-center">
                        <div class="option-item">
                            <i class="search-btn bx bx-search"></i>
                            <i class="close-btn bx bx-x"></i>

                            <div class="search-overlay search-popup">
                                <div class='search-box'>
                                    <form class="search-form">
                                        <input class="search-input" name="search" placeholder="Search" type="text">

                                        <button class="search-button" type="submit">
                                            <i class="bx bx-search"></i>
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="cart-icon">
                            <a href="shopping-cart.html">
                                <i class="bx bx-cart"></i>
                                <span>0</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}
</div>
<!-- End Nav Area -->
