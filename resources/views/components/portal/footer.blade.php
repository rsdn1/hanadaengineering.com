<!-- Start Footer Area -->
<footer class="footer-area pt-100 pb-70">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6">
                <div class="single-footer-widget">
                    <a href="#" class="logo">
                        <img src="{{ asset('img/hanada.jpg') }}" width="150" height="150" alt="Image" class="mb-2">
                        <h3>HANADA M&E <br> SDN BHD</h3>
                    </a>

                    {{-- <p>Lorem ipsum dolor sit amet, consec tetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua consec tetur adipiscing.</p> --}}
                </div>
            </div>

            <div class="col-lg-3 col-md-6">
                <div class="single-footer-widget">
                    <h3>Our services</h3>

                    <ul class="import-link">
                        <li>
                            <a href="#">Pool construction</a>
                        </li>
                        <li>
                            <a href="#">Pool maintenance</a>
                        </li>
                        <li>
                            <a href="#">Pool renovation</a>
                        </li>
                        <li>
                            <a href="#">Pool safety covers</a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-3 col-md-6">
                <div class="single-footer-widget">
                    <h3>Quick link</h3>

                    <ul class="import-link">
                        <li>
                            <a href="#">Pool equipment</a>
                        </li>
                        <li>
                            <a href="#">Terms and conditions </a>
                        </li>
                        <li>
                            <a href="#">Consulting</a>
                        </li>
                        <li>
                            <a href="#">FAQs</a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-3 col-md-6">
                <div class="single-footer-widget">
                    <h3>Contact us</h3>

                    <ul class="address">
                        <li class="location">
                            <i class="bx bxs-location-plus"></i>
                            Lot 041B, Lorong bIstari 1, kg sri aman puchong

                        </li>
                        <li>
                            <i class="bx bxs-envelope"></i>
                            <a href="mailto:hanadaservices@gmail.com">hanadaservices@gmail.com</a>
                        </li>
                        <li>
                            <i class="bx bxs-phone-call"></i>
                            <a href="tel:+603-80611041">+603-80611041</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- End Footer Area -->
