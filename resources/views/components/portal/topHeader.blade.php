<!-- Start Top Header -->
<div class="top-header top-header-three">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-lg-6 col-sm-4">
                <ul class="header-left-content">
                    <li>
                        <a href="#" target="_blank">
                            <i class="bx bxl-facebook"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#" target="_blank">
                            <i class="bx bxl-instagram"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#" target="_blank">
                            <i class="bx bxl-linkedin"></i>
                        </a>
                    </li>
                </ul>
            </div>

            <div class="col-lg-6 col-sm-8">
                <div class="header-right-content">
                    <div class="call-us">
                        <i class="bx bx-envelope"></i>
                        <a href="mailto:hanadaservices@gmail.com">hanadaservices@gmail.com</a>
                    </div>
                    <div class="call-us">
                        <i class="bx bx-phone-call"></i>
                        <a href="tel:+603-80611041">+603-80611041</a>
                    </div>

                    {{-- <ul class="flag-area">
                        <li class="flag-item-top">
                            <a href="#" class="flag-bar">
                                <span>Language</span>
                                <i class="bx bx-chevron-down"></i>
                            </a>

                            <ul class="flag-item-bottom">
                                <li class="flag-item">
                                    <a href="#" class="flag-link">
                                        <img src="{{ asset('img/flag/english.png') }}" alt="Image">
                                        English
                                    </a>
                                </li>
                                <li class="flag-item">
                                    <a href="#" class="flag-link">
                                        <img src="{{ asset('img/flag/arab.png') }}" alt="Image">
                                        العربيّة
                                    </a>
                                </li>
                                <li class="flag-item">
                                    <a href="#" class="flag-link">
                                        <img src="{{ asset('img/flag/germany.png') }}" alt="Image">
                                        Deutsch
                                    </a>
                                </li>
                                <li class="flag-item">
                                    <a href="#" class="flag-link">
                                        <img src="{{ asset('img/flag/portugal.png') }}" alt="Image">
                                        󠁥󠁮󠁧󠁿Português
                                    </a>
                                </li>
                                <li class="flag-item">
                                    <a href="#" class="flag-link">
                                        <img src="{{ asset('img/flag/china.png') }}" alt="Image">
                                        简体中文
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul> --}}
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Start Top Header -->
